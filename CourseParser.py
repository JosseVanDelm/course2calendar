"""
Open the file
Parse the html for
 * Course name
 * Location
 * Day
 * Hours - Start / End
"""

# Import necessary classes:
import os
import lxml.html
from datetime import datetime
import pytz
import icalendar
import argparse
import requests

"""
WARNING: This code only works for academic year 2020 - 2021.
If you wish to use it in another year.
Please edit the check-up that manages the time zone changes due to winter and
summer time.
"""


class Event:
    def __init__(self, day, month, year, starth, startm, stoph, stopm,
                 timezone, summary, location="", description=""):
        self.starttime = datetime(year, month, day, starth, startm,
                                  tzinfo=timezone)
        self.stoptime = datetime(year, month, day, stoph, stopm,
                                 tzinfo=timezone)
        self.summary = summary
        self.location = location
        self.description = description

    def __str__(self):
        string = ("{}\n{}\n{}\nStart: {}\nStop: {}\n@ {}\n{}\n\n"
                  .format("=" * len(self.summary),
                          self.summary,
                          "=" * len(self.summary),
                          str(self.starttime),
                          str(self.stoptime),
                          str(self.location),
                          str(self.description)))
        return string

    def make_ical_event(self):
        ievent = icalendar.Event()
        ievent.add('summary', self.summary)
        ievent.add('dtstart', self.starttime)
        ievent.add('dtend', self.stoptime)
        ievent['location'] = icalendar.vText(self.location)
        ievent['description'] = icalendar.vText(self.description)
        return ievent


def check_for_week_table(table_element):
    # Check if the current table is a line with red text that says the
    # weeknumbers and the day of the week.
    for element in table_element.iter():
        if element.tag == "i":
            if element.text == "Week":
                return True
    return False


def parse_lesson_table(information_indices, tables_list, year, lang="en"):
    # After a week_table two table elements come with course information.
    # 1) Information about the course
    # 2) The dates of the course
    events = []
    for index in range(0, len(information_indices), 2):
        # The odd elements contain 1) Information about the course
        counter = 0
        course_location = ""
        course_name = ""
        course_starth = 0
        course_startm = 0
        course_stoph = 0
        course_stopm = 0
        course_days = []
        course_months = []
        for element in tables_list[information_indices[index]].iter():
            if element.tag == "td" and not element.text.isspace():
                # This is the information about the start and stop hours:
                # e.g. 12:00 to 13:00.
                if counter == 0:
                    if lang == "en":
                        start, stop = element.text.strip().split(" to ")
                    else:  # lang == "nl":
                        start, stop = element.text.strip().split(" tot ")
                    course_starth, course_startm = start.split(":")
                    course_stoph,  course_stopm = stop.split(":")
                # This is(/are) the location(s) of the course.
                if counter == 1:
                    course_location = element.text.strip()
                # This is the name of the course.
                if counter == 2:
                    course_name = element.text.strip()
                # Continue the counting.
                counter += 1
        # The even elements contain 2) The dates of the Course.
        for element in tables_list[information_indices[index+1]].iter():
            if element.tag == "i":
                # Strip all leading and lagging whitespaces.
                day, month = element.text.strip().split(".")
                course_days.append(day)
                course_months.append(month)
        for i in range(len(course_days)):
            # Winter time from 25-10-2019 till 28-3-2020.
            # This should probably be rewritten.
            if ((year == 2020 and int(course_days[i]) >= 25 and
                    int(course_months[i]) >= 10) or
                    (year == 2020 and int(course_months[i]) >= 11) or
                    (year == 2021 and int(course_days[i]) <= 28 and
                        int(course_months[i]) <= 3) or
                    (year == 2021 and int(course_months[i]) <= 2)):
                timezone = pytz.timezone("Etc/GMT-1")
            # Summer time
            else:
                timezone = pytz.timezone("Etc/GMT-2")
            events.append(Event(int(course_days[i]), int(course_months[i]),
                                int(year), int(course_starth),
                                int(course_startm), int(course_stoph),
                                int(course_stopm), timezone, course_name,
                                course_location))
    return events


def main():
    parser = argparse.ArgumentParser(description="iCal-converter for KU Leuven"
                                                 " semestrial HTML course"
                                                 " schedules.")
    parser.add_argument('year', help="Year of the parsed schedule", type=int)
    parser.add_argument('--input', help="path to HTML input file", type=str)
    parser.add_argument('--url', help="path to HTML url", type=str)
    parser.add_argument('--output', help="path to output iCal-file",
                        default=None, type=str)
    parser.add_argument("--lang", help="Language of the HTML-document",
                        default="en", type=str)
    args = parser.parse_args()
    # Set the input file name.
    if args.input:
        input_file = args.input
    elif args.url:
        # Curl the input file.
        data = requests.post(args.url)
        fp = open("./schedule.html", "w")
        fp.write(data.text)
        fp.close
        # Stupid to close the file if it gets opened anyway, but it works.
        input_file = "schedule.html"
    else:
        raise ValueError("No input file or url was given. Supply one.")

    # Set output destination.
    if args.output:
        output_file = args.output
    else:
        filename = input_file.split(".")[-1]
        output_file = filename + ".ics"
    # Open the file:
    with open(input_file) as HTML_file:
        root = lxml.html.parse(HTML_file)
        # All information is contained in Table tags.
        # Here is some information on what these tables contain:
        # 1) Empty table for spacing;
        # 2) Title of the List;
        # 3) Buttons for printing and other schedule actions;
        # 4) Colored titlebox with field names (Time,Building,...).
        tables = []
        # Make a list of all table elements in the HTML document.
        for element in root.iter():
            if element.tag == "table":
                tables.append(element)
        # Get the indices of the week table elements.
        i = 0
        week_table_indices = []
        for element in tables:
            if check_for_week_table(element):
                week_table_indices.append(i)
            i += 1
        # Get the table elements that lag two elements behind the week table
        # elements.
        # These are the indices of the tables with course information.
        number_of_indices = len(week_table_indices)
        i = 0
        information_table_indices = []
        for index in week_table_indices:
            if i == number_of_indices-1:
                # You have arrived at the last index
                # --> special information index calculation.
                for number in range(week_table_indices[i]+2, len(tables)-1):
                    information_table_indices.append(number)
            else:
                # Not yet at last index.
                for number in range(week_table_indices[i]+2,
                                    week_table_indices[i+1]):
                    information_table_indices.append(number)
            i += 1
        # parse all information tables
        events = parse_lesson_table(information_table_indices, tables,
                                    args.year, args.lang)
        cal = icalendar.Calendar()
        # These lines are necessary to make the iCal-file valid.
        cal.add('prodid', '-//CourseParseCalendar//')
        cal.add('version', '2.0')
        # Add in the evens
        for event in events:
            print(event)
            cal.add_component(event.make_ical_event())
        # Show the user how many events were found
        answer = input(str(len(events))+" events found. Write to file? (Y/n)")
        if answer == "Y" or answer == "y" or not answer:
            with open(output_file, 'wb') as f:
                f.write(cal.to_ical())
        else:
            exit(1)


main()
